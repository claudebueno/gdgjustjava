# GDG Just Java #

Study Jams Android April 2016

## What is this repository for? ##

* A simple App to learn how to programming Android mobile applications
* v. 1.0.0

### Study Jams Android ###
* Learn the basics of Android and Java programming, and take the first step on your journey to becoming an Android developer
* [Udacity - Android Development for Beginners ](https://www.udacity.com/course/android-development-for-beginners--ud837)

### Who I am? ###

* Claude BUENO
* [www.claudebueno.com](http://www.claudebueno.com)
